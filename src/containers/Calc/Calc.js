import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Calc.css';

const Calc = () => {
    const dispatch = useDispatch()
    const counter = useSelector(state => state.counter );

    const addNumber = (e) => dispatch({type: 'AddNumber', payload: e.target.name})
    const addSymbol = (e) => dispatch({type: 'AddSymbol', payload: e.target.name})
    const result = () => dispatch({type: 'Result'});
    const remove = () => dispatch({type: 'Remove'})

    return (
        <div className="column">
            <div className="general">
                <input type="text" value={counter} readOnly/>
            </div>
            <div className="row">
               <button onClick={remove}>AC</button>
               <button name='+/-' onClick={addSymbol}>+/-</button>
               <button name='%' onClick={addSymbol}>%</button>
               <button name='/' onClick={addSymbol}>/</button>
            </div>
            <div className="row">
               <button name='7' onClick={addNumber}>7</button>
               <button name="8" onClick={addNumber}>8</button>
               <button name='9' onClick={addNumber}>9</button>
               <button name='*' onClick={addSymbol}>*</button>
            </div>
            <div className="row">
                <button name='4' onClick={addNumber}>4</button>
                <button name='5' onClick={addNumber}>5</button>
                <button name='6' onClick={addNumber}>6</button>
                <button name='-' onClick={addSymbol}>-</button>
            </div>
            <div className="row">
                <button name='1' onClick={addNumber}>1</button>
                <button name='2' onClick={addNumber}>2</button>
                <button name='3' onClick={addNumber}>3</button>
                <button name='+' onClick={addSymbol}>+</button>
            </div>
            <div className="row">
                <button name='0' onClick={addNumber} className='extra'>0</button>
                <button name=',' onClick={addSymbol}>,</button>
                <button onClick={result}>=</button>
            </div>
        </div>
    );
};

export default Calc;