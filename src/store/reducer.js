const initialState = {
    counter : 0,
    input: 0,
};

const reducer = (state = initialState, action) => {
    if(action.type === 'AddNumber'){
        if(state.counter === 0){
            return {...state, counter: action.payload}
        }
        return {...state, counter: state.counter + action.payload}
    }
    if(action.type === 'AddSymbol'){
        if(state.counter === 0){
            alert('Type the first Number')
            return {...state, counter: 0}
        }

        if(action.payload === '+/-'){
            alert('I could not realize for this symbol method')
            return {...state, counter: 0}
        }
        return {...state, counter: state.counter.toString() + ' ' + action.payload + ' '}
    }

    if(action.type ==='Result'){
        const number = eval(state.counter)
        return {...state, counter: number}
    }

    if(action.type === 'Remove'){
        return {...state, counter: 0}
    }

    return state;
}

export default  reducer;